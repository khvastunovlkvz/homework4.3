public class Dog extends Animal{

    public Dog(String color, int x, int y){
        super(color, x, y);
        System.out.println();
        System.out.println("Создан объект Dog");
        System.out.println("Положение х = " + x);
        System.out.println("Положение у = " + y);
        System.out.println("Цвет собаки : " + color);
        System.out.println();
    }

}
