public class Cat extends Animal{

    public Cat(String color, int x, int y){
        super(color, x, y);
        System.out.println();
        System.out.println("Создан объект Cat");
        System.out.println("Положение х = " + x);
        System.out.println("Положение у = " + y);
        System.out.println("Цвет кошки : " + color);
        System.out.println();
    }
}
