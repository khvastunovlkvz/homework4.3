
import java.util.*;

public class Mein {

    public static void main(String[] args) {

        RectangleClass rectangle = new RectangleClass("Blue",10,10);        //Создан объект прямоугольник

        RoundClass round = new RoundClass("Green", 5);      //Создан объект круг

        TriangleClass triangle = new TriangleClass("Yellow", 10, 10, 10);       //Создан объект треугольник

        Cat cat = new Cat("Black", 0,0);        //Создан объект кошка

        Dog dog = new Dog("White", 0, 0);       //Создан объект собака

        ArrayList <Movable> list = new ArrayList<>();       //Создана коллекция объектов интерфейса Movable

        list.add(0, rectangle);     //В коллекцию добавлен прямоугольник
        list.add(1, round);         //В коллекцию добавлен круг
        list.add(2, triangle);      //В коллекцию добавлен треугольник
        list.add(3, cat);           //В коллекцию добавлена кошка
        list.add(4, dog);           //В коллекцию добавлена собака

        System.out.println(list.size());  //Текущий размер коллекции


        int i = 0;
        while (i < list.size()) {     //Цикл от нуля до текущего размера коллекции
            list.get(i).moveRight(10);                  //Сдвиг вправо на 10 объектов коллекции
            list.get(i).getCoordinates();               //Проверка положения объектов
            i++;
        }
        //
        //Сначала был цикл for (int i = 0 ; i < list.size; i++),
        //но ИДЕА предложила цикл через while. В чем разница?
        //




    }
}
