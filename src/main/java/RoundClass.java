public class RoundClass extends Figure {

    public RoundClass(){                                    //Пустой конструктор
        System.out.println("Создан круг");
    }


    public RoundClass(int radius){                          //Конструктор инициализация радиуса
        this.radius = radius;
        System.out.println("Создан круг");
        System.out.println("Задан радиус : " + radius);

    }

    public RoundClass(String color, int radius){            //Конструктор инициализация цвета и радиуса
        super(color);
        System.out.println("Создан круг");
        System.out.println("Цвет круга : " + color);
        this.radius = radius;
        System.out.println("Задан радиус : " + radius);

    }
    int radius;

    @Override
    public void getArea(){                                //Метод расчет площади
        double area = Math.PI * radius * radius;
        System.out.println( "Площадь круга : " + area);
    }

    @Override
    public void getLength(){                              //Метод расчет длины
        double length = 2 * Math.PI * radius;
        System.out.println("Длина круга : " + length);
    }
}
