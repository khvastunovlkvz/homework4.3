public abstract class Animal implements Movable {

    private int x;
    private int y;
    private String color;

    public Animal(){                                //Пустой конструктор
        System.out.println();
        System.out.println("Запуск конструктора Animal");
        System.out.println("Конструктор Animal завершен");
        System.out.println();
    }

    public Animal(String color, int x, int y){      //Конструктор Animal с инициализацией color и координат x, y
        this.color = color;
        this.x = x;
        this.y = y;
        System.out.println();
        System.out.println("Запуск конструктора Animal");
        System.out.println("Инициализация координат : x = " + x + "; y = " + y);
        System.out.println("Цвет : " + color);
        System.out.println("Конструктор Animal завершен");
        System.out.println();
    }

    public Animal(String color) {                       //Конструктор Animal с инициализацией color
        this.color = color;
        System.out.println();
        System.out.println("Запуск конструктора Animal");
        System.out.println("Цвет : " + color);
        System.out.println("Конструктор Animal завершен");
        System.out.println();
    }


    @Override
    public void moveLeft(int left){                              //Метод сдвиг влево
        if (left >= 0) {        //Запрет ввода отрицательных чисел
            x -= left;
            System.out.println("Объект сдвинут влево на: " + left);
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }
    }

    @Override
    public void moveRight(int right){                            //Метод сдвиг вправо
        if(right >= 0) {        //Запрет ввода отрицательных чисел
            x += right;
            System.out.println("Объект сдвинут вправо на: " + right);
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }
    }

    @Override
    public void moveUp(int up){                                  //Метод сдвиг вверх
        if (up >= 0) {          //Запрет ввода отрицательных чисел
            y += up;
            System.out.println("Объект сдвинут вверх на: " + up);
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }
    }

    @Override
    public void moveDown(int down){                              //Метод сдвиг вниз
        if (down >= 0) {        //Запрет ввода отрицательных чисел
            y -= down;
            System.out.println("Объект сдвинут вниз на: " + down);
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }
    }

    @Override
    public void getCoordinates(){                               //Метод инициализация конечных координат
        System.out.println("Текущее положение животного по координатам : x = " + x + "; y = " + y);
    }
}
