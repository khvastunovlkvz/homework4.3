public interface Movable {

    void moveLeft(int left);        //Метод сдвиг влево
    void moveRight(int right);      //Метод сдвиг вправо
    void moveUp(int up);            //Метод сдвиг вверх
    void moveDown(int down);        //Метод сдвиг вниз
    void getCoordinates();          //Метод вывод конечных координат






}
